!/bin/bash

# Make installer interactive and select normal mode by default.
        INTERACTIVE="y"
ADVANCED="n"
I2PREADY="n"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
-a|--advanced)
ADVANCED="y"
shift
;;
-n|--normal)
ADVANCED="n"
FAIL2BAN="y"
UFW="y"
BOOTSTRAP="y"
shift
;;
-i|--externalip)
EXTERNALIP="$2"
ARGUMENTIP="y"
shift
        shift
;;
--bindip)
BINDIP="$2"
shift
        shift
;;
-k|--privatekey)
KEY="$2"
shift
        shift
;;
-f|--fail2ban)
FAIL2BAN="y"
shift
;;
--no-fail2ban)
FAIL2BAN="n"
shift
;;
-u|--ufw)
UFW="y"
shift
;;
--no-ufw)
UFW="n"
shift
;;
-b|--bootstrap)
BOOTSTRAP="y"
shift
;;
--no-bootstrap)
BOOTSTRAP="n"
shift
;;
--no-interaction)
INTERACTIVE="n"
shift
;;
--tor)
TOR="y"
shift
;;
--i2p)
I2P="y"
shift
;;
-h|--help)
cat << EOL
        Bulwark Masternode installer arguments:
-n --normal               : Run installer in normal mode
-a --advanced             : Run installer in advanced mode
-i --externalip <address> : Public IP address of VPS
--bindip <address>        : Internal bind IP to use
-k --privatekey <key>     : Private key to use
-f --fail2ban             : Install Fail2Ban
--no-fail2ban             : Do nott install Fail2Ban
-u --ufw                  : Install UFW
--no-ufw                  : Do not install UFW
-b --bootstrap            : Sync node using Bootstrap
--no-bootstrap            : Do not use Bootstrap
-h --help                 : Display this help text.
--no-interaction          : Do not wait for wallet activation.
--tor                     : Install TOR and configure bulwarkd to use it
--i2p                     : Install I2P (Requires 2GB of RAM)
EOL
        exit
;;
*)    # unknown option
POSITIONAL+=("$1") # save it in an array for later
        shift
;;
esac
        done
set -- "${POSITIONAL[@]}" # restore positional parameters

        clear

# Make sure curl is installed
apt-get update
apt-get install -qqy curl jq
        clear

# These should automatically find the latest version of Bulwark